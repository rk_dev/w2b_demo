/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function bt_play(a: number, b: number, c: number, d: number, e: number, f: number): void;
export function bt_send_message(a: number, b: number, c: number, d: number): void;
export function bt_stop(): void;
export function bt_set_speed(a: number): void;
export function bt_process(a: number, b: number, c: number, d: number, e: number, f: number): void;
export function __wbindgen_malloc(a: number, b: number): number;
