/* tslint:disable */
/* eslint-disable */
/**
* @param {Uint8Array} sf2_file_src
* @param {Uint8Array} midi_file_src
*/
export function bt_play(sf2_file_src: Uint8Array, midi_file_src: Uint8Array): void;
/**
* @param {number} channel
* @param {number} command
* @param {number} data1
* @param {number} data2
*/
export function bt_send_message(channel: number, command: number, data1: number, data2: number): void;
/**
*/
export function bt_stop(): void;
/**
* @param {number} speed
*/
export function bt_set_speed(speed: number): void;
/**
* @param {Float32Array} left
* @param {Float32Array} right
*/
export function bt_process(left: Float32Array, right: Float32Array): void;

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly bt_play: (a: number, b: number, c: number, d: number, e: number, f: number) => void;
  readonly bt_send_message: (a: number, b: number, c: number, d: number) => void;
  readonly bt_stop: () => void;
  readonly bt_set_speed: (a: number) => void;
  readonly bt_process: (a: number, b: number, c: number, d: number, e: number, f: number) => void;
  readonly __wbindgen_malloc: (a: number, b: number) => number;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {SyncInitInput} module
*
* @returns {InitOutput}
*/
export function initSync(module: SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
