let ac;
let midiSound;
export let baseVolume = 0.5;
let vol;

export async function initialize(){
    const wasmBuffer = await fetch('./lib/benten/benten_bg.wasm').then((wasm) => wasm.arrayBuffer());
    const audioContext = await new AudioContext({latencyHint: "balanced", sampleRate: 44100}); // 携帯で正常に再生するために指定必須
    await audioContext.audioWorklet.addModule('./lib/midisound.js');

    midiSound = new AudioWorkletNode(audioContext, "midi-sound-processor");
    vol = new GainNode(audioContext, {gain: baseVolume});
    midiSound.port.postMessage({type:"init_wasm", wasm: wasmBuffer});
    
    midiSound.connect(vol).connect(audioContext.destination);
    ac = audioContext;

    return Promise.resolve();
}

async function ensure_initialize(){
    while(!ac){    
        await setTimeout(() => {}, 200);
    }
}
export async function playBuffer(sf2Buffer, smfBuffer){
    await ensure_initialize()
    midiSound.port.postMessage({type:"play", sf2:sf2Buffer, smf:smfBuffer, sampleRate:ac.sampleRate});
}
export async function play(filename){
    await ensure_initialize()
    fetch(filename).then((smf) => smf.arrayBuffer()).then((smfBuffer) => {
        const u8SmfBuffer = new Uint8Array(smfBuffer);
        playBuffer(u8SmfBuffer);
    });
}
export async function stop(){
    await ensure_initialize()
    midiSound.port.postMessage({type:"stop"});
}
export async function setTempo(speed){
    await ensure_initialize()
    midiSound.port.postMessage({type:"set_speed", speed});
}
export async function setVolume(volume){
    await ensure_initialize()
    vol.gain.value = volume * baseVolume;
}